# Advent of Code 2020

This is my participation to Advent of Code 2020, written in Rust. Unfortunately, I didn't have the time nor the motivation to finish, so only the days up to the 16th have been done.

My main objective was to practice Rust, so while I tried to write the best code possible with my limited time and my limited knowledge of Rust, it's very likely that there are more appropriate/efficient/idiomatic ways to do what I did.

Type `cargo run` on the command line to get the solutions. (You can also use `cargo test` to test the implementation against the examples given by Advent of Code.)

There are no external dependencies. One consequence is that a lot of things that would have been very easy with regular expressions are done manually.

Also, the code assumes that every input is well formed and errors are not handled. As a result, the program will panic if an input is malformed.

The solutions to each puzzle are written in the modules `days::day1`, `days::day2`, and so on. Each of these modules contains a `solve` function that prints the solution.

The folder `resources` contains my input files.
