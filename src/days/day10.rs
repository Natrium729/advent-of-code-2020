//! Solves the day 9.
//!
//! https://adventofcode.com/2020/day/9

use std::fs;

pub fn solve() {
    let f = fs::read_to_string("resources/joltage-adapters.txt")
        .expect("joltage-adapters.txt does not exist");
    let mut joltages = f.lines()
        .map(|x| x.parse::<u32>().unwrap())
        .collect::<Vec<_>>();
    joltages.sort_unstable();

    let diffs = count_joltage_diffs(&joltages);
    println!("Answer 10.1: {}", diffs.0 * diffs.2);
    println!("Answer 10.2: {}", count_combinations(&joltages));
}

/// Count the number time there is a differences of 1, 2 or 3 between consecutive entries.
/// (It is assumed that the joltages are already sorted.)
///
/// A tuple containing the number times there's a difference of 1, 2 amd 3 is returned.
///
/// Panics if there is a difference lower than 1 or higher than 3.
fn count_joltage_diffs(joltages: &[u32]) -> (u32, u32, u32) {
    let mut diffs = [0u32; 3];
    let mut last_joltage = 0;
    for &joltage in joltages {
        let diff = joltage - last_joltage;
        if diff < 1 || diff > 3 {
            panic!("invalid joltage difference of {} (should be in 1..=3)", diff);
        }
        last_joltage = joltage;
        diffs[(diff - 1) as usize] += 1;
    }
    diffs[2] += 1; // Account for your device's built-in adapter, which is 3 volts higher than the last.
    (diffs[0], diffs[1], diffs[2])
}

/// Calculate the length of runs of differences of 1 between each two consecutive entries.
///
/// That is, split the slice each time there is a difference of 3 between an entry and it
/// predecessor, and return a vector of the length of these parts.
fn count_runs_of_one_diffs(joltages: &[u32]) -> Vec<u32> {
    let mut runs = Vec::new();
    let mut last_joltage = 0;
    let mut count = 0;
    for &joltage in joltages {
        let diff = joltage - last_joltage;
        match diff {
            1 => count += 1,
            3 => {
                runs.push(count);
                count = 0;
            },
            _ => panic!("invalid value when counting runs of ones: {}", diff)
        }
        last_joltage = joltage;
    }
    runs.push(count); // Account for your device's built-in adapter, which is 3 volts higher than the last.
    runs
}

/// Count the different possible ways we can arrange the joltage adapters (by not using
/// some of them).
///
/// It only works when there are no difference of 2 between two consecutive adapters. But
/// luckily, we don't have any in our puzzle! ;)
fn count_combinations(joltages: &[u32]) -> u64 {
    // For each (disjointed) runs of differences of 1, we count the number of possible
    // combinations for it and multiply the resulting numbers.
    // (It works because there are no differences of 2.)
    count_runs_of_one_diffs(&joltages).iter()
        .map(|&run_length| {
            // The formula wrong for numbers above 4, but we don't have any in the puzzle.

            // If there are two differences of 3 one after the other, we just return 1.
            // (There's only one possible "combination".)
            if run_length == 0 {
                return 1
            }
            // There's 2^(run_length - 1) combinations. (minus 1 because we can't remove
            // the last adapter of the run, since the adapter after it has a difference of 3.
            // Removing it would thus result in a difference greater than 3.
            let mut result = 2u64.pow(run_length - 1).max(1);

            // But we can't remove 3 or more consecutive adapters, since it would result in a
            // difference greater than 3. So we remove these invalid combinations.
            // In fact the formula only works for numbers up to 4, but it'll do for our puzzle.
            if run_length >= 3 {
                result -= run_length as u64 - 3
            }
            result
        })
        .product()

}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test() {
        let mut joltages = [16, 10, 15, 5, 1, 11, 7, 19, 6, 12, 4];
        joltages.sort_unstable();
        let diffs = count_joltage_diffs(&joltages);
        assert_eq!(diffs.0, 7);
        assert_eq!(diffs.2, 5);
        assert_eq!(count_combinations(&joltages), 8);


        let mut joltages = [28, 33, 18, 42, 31, 14, 46, 20, 48, 47, 24, 23, 49, 45, 19, 38, 39, 11, 1, 32, 25, 35, 8, 17, 7, 9, 4, 2, 34, 10, 3];
        joltages.sort_unstable();
        let diffs = count_joltage_diffs(&joltages);
        assert_eq!(diffs.0, 22);
        assert_eq!(diffs.2, 10);
        assert_eq!(count_combinations(&joltages), 19208);
    }
}
