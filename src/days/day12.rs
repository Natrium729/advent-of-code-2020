//! Solves the day 12.
//!
//! https://adventofcode.com/2020/day/12

use std::fs;

pub fn solve() {
    let instructions = fs::read_to_string("resources/navigation-instructions.txt")
        .expect("navigation-instructions.txt does not exist");
    let mut ship = Ship::new();
    let mut waypoint_ship = WaypointShip::new();
    for line in instructions.lines() {
        ship.execute_instruction(line);
        waypoint_ship.execute_instruction(line);
    }
    println!("Answer 12.1: {}", ship.distance_from_origin());
    println!("Answer 12.2: {}", waypoint_ship.distance_from_origin());
}

/// A ship that moves according to its position.
#[derive(Debug)]
struct Ship {
    direction: i32,
    // North is in the direction of positive x.
    x: i32,
    y: i32,
}

impl Ship {
    /// Creates a ship at (0, 0).
    fn new() -> Ship {
        Ship {
            direction: 0,
            x: 0,
            y: 0,
        }
    }

    /// Moves the ship according to an instruction in the form "(N|S|E|W|L|R|F)\d+".
    fn execute_instruction(&mut self, instruction: &str) {
        let (action, value) = instruction.split_at(1);
        let value = value.parse::<i32>().unwrap();
        match action {
            "E" => self.x += value,
            "W" => self.x -= value,
            "N" => self.y += value,
            "S" => self.y -= value,
            "F" if self.direction == 0 => self.x += value,
            "F" if self.direction == 90 => self.y += value,
            "F" if self.direction == 180 => self.x -= value,
            "F" if self.direction == 270 => self.y -= value,
            "L" => self.direction = (self.direction + value) % 360,
            // Won't work if we turn more than 360°
            "R" => self.direction = (self.direction + 360 - value) % 360,
            _ => panic!("invalid instruction: {}{}", action, value),
        }
    }

    /// Calculates the Manhattan distance from the ship to the origin.
    fn distance_from_origin(&self) -> i32 {
        self.x.abs() + self.y.abs()
    }
}

/// A ship that moves according to a waypoint (a vector relative to its position).
#[derive(Debug)]
struct WaypointShip {
    // North is in the direction of positive x.
    x: i32,
    y: i32,
    waypoint_x: i32,
    waypoint_y: i32,
}

impl WaypointShip {
    /// Creates a ship at (0, 0) with a waypoint at (10, 1).
    fn new() -> WaypointShip {
        WaypointShip {
            x: 0,
            y: 0,
            waypoint_x: 10,
            waypoint_y: 1,
        }
    }

    /// Moves the ship according to an instruction in the form "(N|S|E|W|L|R|F)\d+".
    fn execute_instruction(&mut self, instruction: &str) {
        let (action, value) = instruction.split_at(1);
        let value = value.parse::<i32>().unwrap();
        match action {
            "E" => self.waypoint_x += value,
            "W" => self.waypoint_x -= value,
            "N" => self.waypoint_y += value,
            "S" => self.waypoint_y -= value,
            "F" => {
                self.x += self.waypoint_x * value;
                self.y += self.waypoint_y * value;
            },
            "L" => {
                for _i in 0..(value / 90) {
                    let (new_x, new_y) = (-self.waypoint_y, self.waypoint_x);
                    self.waypoint_x = new_x;
                    self.waypoint_y = new_y;
                }
            },
            "R" => {
                for _i in 0..(value / 90) {
                    let (new_x, new_y) = (self.waypoint_y, -self.waypoint_x);
                    self.waypoint_x = new_x;
                    self.waypoint_y = new_y;
                }
            },
            _ => panic!("invalid instruction: {}{}", action, value),
        }
    }

    /// Calculates the Manhattan distance from the ship to the origin.
    fn distance_from_origin(&self) -> i32 {
        self.x.abs() + self.y.abs()
    }

}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test() {
        let mut ship = Ship::new();
        let mut waypoint_ship = WaypointShip::new();
        let actions = "F10\n\
            N3\n\
            F7\n\
            R90\n\
            F11";
        for line in actions.lines() {
            ship.execute_instruction(line);
            waypoint_ship.execute_instruction(line);
        }
        assert_eq!(ship.distance_from_origin(), 25);
        assert_eq!(waypoint_ship.distance_from_origin(), 286);
    }
}
