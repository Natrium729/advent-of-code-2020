//! Solves the day 6.
//!
//! https://adventofcode.com/2020/day/6

use std::collections::{HashMap, HashSet};
use std::fs;

pub fn solve() {
        let declarations = fs::read_to_string("resources/customs-declarations.txt")
            .expect("customs-declarations.txt does not exist");
        let yes_count_sum = sum_yes_of_declarations(&declarations[..]);
        println!("Answer 6.1: {}", yes_count_sum.0);
        println!("Answer 6.2: {}", yes_count_sum.1);
}

/// Return a HashSet of questions that were answered yes at least once in a group.
fn yes_answers_in_group(group: &str) -> HashSet<char> {
    group.lines()
        .map(|line| {
            line.chars()
        })
        .flatten()
        .collect::<HashSet<_>>() // Collect in a HashSet to deduplicate
}

/// Return a HashSet of questions that were answered yes by everyone in a group.
fn yes_answers_for_everyone_in_group(group: &str) -> HashSet<char> {
    let mut yes_counts = HashMap::new();
    let mut group_size: u32 = 0;
    for line in group.lines() {
        group_size += 1;
        for ch in line.chars() {
            let char_count = yes_counts.entry(ch).or_insert(0u32);
            *char_count += 1;
        }
    }

    yes_counts.iter()
        .filter(|&(&_k, &v)| v == group_size)
        .map(|(&k, _v)| k)
        .collect()
}

/// Return a tuple containing the sum of numbers of questions answered yes by at least one
/// person in a group by everyone in group, for the given groups.
fn sum_yes_of_declarations(groups: &str) -> (usize, usize) {
    groups.split("\n\n") // We assume there are no CRLF.
        .fold((0, 0), |acc, group| (
            acc.0 + yes_answers_in_group(group).len(),
            acc.1 + yes_answers_for_everyone_in_group(group).len()
        ))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_yes_answers_in_group() {
        let group_answers = yes_answers_in_group("abcx\nabcy\nabcz");
        assert!(group_answers.contains(&'a'));
        assert!(group_answers.contains(&'b'));
        assert!(group_answers.contains(&'c'));
        assert!(group_answers.contains(&'x'));
        assert!(group_answers.contains(&'y'));
        assert!(group_answers.contains(&'z'));
        assert_eq!(group_answers.len(), 6)
    }

    #[test]
    fn test_sum_yes_of_declarations() {
        let declarations = "abc\n\
            \n\
            a\n\
            b\n\
            c\n\
            \n\
            ab\n\
            ac\n\
            \n\
            a\n\
            a\n\
            a\n\
            a\n\
            \n\
            b";
        assert_eq!(sum_yes_of_declarations(declarations).0, 11);
        assert_eq!(sum_yes_of_declarations(declarations).1, 6);
    }
}
