//! Solves the day 11.
//!
//! https://adventofcode.com/2020/day/11

use std::{fmt, fs};

pub fn solve() {
    let string = fs::read_to_string("resources/seats.txt")
        .expect("seats.txt does not exist");

    let mut grid = Grid::from_string(&string);
    grid.update_until_stable();
    println!("Answer 11.1: {}", grid.count_total_occupied());

    let mut grid = Grid::from_string(&string);
    grid.update_v2_until_stable();
    println!("Answer 11.2: {}", grid.count_total_occupied());
}

/// The states of a cell.
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
enum Cell {
    Floor,
    EmptySeat,
    OccupiedSeat,
}

/// A grid of cells
///
/// `cells` contained the cells flattened into an single vector.
struct Grid {
    cells: Vec<Cell>,
    height: usize,
    width: usize,
}

impl Grid {
    /// Creates a `Grid` from a string.
    fn from_string(string: &str) -> Grid {
        let cells = string.lines()
            .map(|row| { // We won't check if each row has the same length.
                row.chars().map(|cell| match cell {
                    '.' => Cell::Floor,
                    'L' => Cell::EmptySeat,
                    '#' => Cell::OccupiedSeat,
                    _ => panic!("invalid character: {}", cell),
                })
                .collect::<Vec<Cell>>()
            })
            .flatten()
            .collect();
        let width = string.lines().next().unwrap().bytes().len();
        let height = string.lines().count();
        Grid {
            cells,
            height,
            width
        }
    }

    /// Gets the index of a cell (x, y) in `self.cells`.
    fn get_index(&self, x: usize, y: usize) -> usize{
        x + (y * self.width)
    }

    /// Counts the number of occupied seats next to the given coordinates.
    fn count_occupied_neighbours(&self, x: usize, y: usize) -> usize {
        let mut count = 0;
        for &delta_x in [self.width - 1, 0, 1].iter() {
            for &delta_y in [self.height - 1, 0, 1].iter() {
                if delta_x == 0 && delta_y == 0 { // That's the cell we are checking.
                    continue;
                }
                if x == 0 && delta_x == self.width - 1 || x == self.width - 1 && delta_x == 1 || y == 0 && delta_y == self.height - 1 || y == self.height - 1 && delta_y == 1 { // We're on an edge.
                    continue;
                }
                let checked_x = (x + delta_x) % self.width;
                let checked_y = (y + delta_y) % self.height;
                let index = self.get_index(checked_x, checked_y);
                if let Cell::OccupiedSeat = self.cells[index] {
                    count += 1;
                }
            }
        }
        count
    }

    /// Counts the number of occupied seats visible in the eight directions from the given
    /// coordinates.
    fn count_occupied_visible(&self, x: usize, y: usize) -> u32 {
        [
            self.is_occupied_visible(x, y, 1, 0) as u32,
            self.is_occupied_visible(x, y, -1, 0) as u32,
            self.is_occupied_visible(x, y, 0, 1) as u32,
            self.is_occupied_visible(x, y, 0, -1) as u32,
            self.is_occupied_visible(x, y, 1, 1) as u32,
            self.is_occupied_visible(x, y, 1, -1) as u32,
            self.is_occupied_visible(x, y, -1, 1) as u32,
            self.is_occupied_visible(x, y, -1, -1) as u32,
        ].iter().sum()

    }

    /// Determines if there is an occupied seat visible from the given coordinates in the
    /// given direction.
    ///
    /// Only the sign of `x_dir` and `y_dir` matters, not their value (i.e. any negative
    /// `y_dir` means down).
    fn is_occupied_visible(&self, x: usize, y: usize, x_dir: i32, y_dir: i32) -> bool {
        let mut checked_x = x;
        let mut checked_y = y;
        loop {
            // Check if we reached an edge
            if checked_x == 0 && x_dir < 0
            || checked_x == self.width - 1 && x_dir > 0
            || checked_y == 0 && y_dir < 0
            || checked_y == self.height - 1 && y_dir > 0 {
                return false;
            }

            // These operations will always be OK since we've checked if we reached the edges.
            if x_dir > 0 {
                checked_x += 1;
            } else if x_dir < 0 {
                checked_x -= 1;
            }
            if y_dir > 0 {
                checked_y += 1;
            } else if y_dir < 0 {
                checked_y -= 1;
            }

            let index = self.get_index(checked_x, checked_y);
            match self.cells[index] {
                Cell::EmptySeat => return false,
                Cell::OccupiedSeat => return true,
                _ => (),
            }
        }
    }

    /// Updates the grid to the next iteration.
    ///
    /// An empty seat with no nearby occupied seat becomes occupied.
    /// An occupied seat with more thant 4 occupied seat nearby becomes empty.
    /// Other cells don't change.
    fn update(&mut self) {
        let mut new_cells = self.cells.clone();
        for x in 0..self.width {
            for y in 0..self.height {
                let index = self.get_index(x, y);
                let cell = self.cells[index];
                let occupied_count = self.count_occupied_neighbours(x, y);
                new_cells[index] = match (cell, occupied_count) {
                    (Cell::EmptySeat, count) if count == 0 => Cell::OccupiedSeat,
                    (Cell::OccupiedSeat, count) if count >= 4 => Cell::EmptySeat,
                    (other, _) => other,
                };
            }
        }
        self.cells = new_cells;
    }

    /// Updates the grid to the next iteration until it doesn't change anymore.
    fn update_until_stable(&mut self) {
        loop {
            let previous_cells = self.cells.clone();
            self.update();
            if self.cells == previous_cells {
                return;
            }
        }
    }

    /// Updates the grid to the next iteration, using the second version of the rules.
    ///
    /// An empty seat with no visible occupied seat from it becomes occupied.
    /// An occupied seat with more thant 5 occupied seat visible becomes empty.
    /// Other cells don't change.
    fn update_v2(&mut self) {
        let mut new_cells = self.cells.clone();
        for x in 0..self.width {
            for y in 0..self.height {
                let index = self.get_index(x, y);
                let cell = self.cells[index];
                let occupied_count = self.count_occupied_visible(x, y);
                new_cells[index] = match (cell, occupied_count) {
                    (Cell::EmptySeat, count) if count == 0 => Cell::OccupiedSeat,
                    (Cell::OccupiedSeat, count) if count >= 5 => Cell::EmptySeat,
                    (other, _) => other,
                };
            }
        }
        self.cells = new_cells;
    }

    /// Updates the grid (with the second version of the rules) to the next iteration until it
    /// doesn't change anymore.
    fn update_v2_until_stable(&mut self) {
        loop {
            let previous_cells = self.cells.clone();
            self.update_v2();
            if self.cells == previous_cells {
                return;
            }
        }
    }

    /// Count the total number of occupied seats in the grid.
    fn count_total_occupied(&self) -> usize {
        self.cells.iter()
            .filter(|&&cell| cell == Cell::OccupiedSeat)
            .count()
    }
}

// To show the grid as a string when printed.
impl fmt::Display for Grid {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for line in self.cells.as_slice().chunks(self.width) {
            for cell in line {
                match cell {
                    Cell::EmptySeat => write!(f, "L")?,
                    Cell::OccupiedSeat => write!(f, "#")?,
                    Cell::Floor => write!(f, ".")?,
                }
            }
            writeln!(f)?;
        }
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test() {
        let string = "L.LL.LL.LL\n\
            LLLLLLL.LL\n\
            L.L.L..L..\n\
            LLLL.LL.LL\n\
            L.LL.LL.LL\n\
            L.LLLLL.LL\n\
            ..L.L.....\n\
            LLLLLLLLLL\n\
            L.LLLLLL.L\n\
            L.LLLLL.LL";

        let mut grid = Grid::from_string(string);
        grid.update_until_stable();
        assert_eq!(grid.count_total_occupied(), 37);

        let mut grid = Grid::from_string(string);
        grid.update_v2_until_stable();
        assert_eq!(grid.count_total_occupied(), 26);
    }
}
