//! Solves the day 1.
//!
//! https://adventofcode.com/2020/day/1

use std::fs;

pub fn solve() {
    let entries = fs::read_to_string("resources/expense-report.txt")
        .expect("expense-report.txt does not exist")
        .lines()
        .map(|x| x.trim().parse::<u32>().unwrap())
        .collect::<Vec<_>>();
    let (x, y) = find_sum_2(2020, &entries).unwrap();
    println!("Answer 1.1: {}", x * y);
    let (x, y, z) = find_sum_3(2020, &entries).unwrap();
    println!("Answer 1.2: {}", x * y * z);
}

/// Find the two numbers in the slice that sum to `result`.
///
/// Returns an option containing these numbers, or None if none were found.
fn find_sum_2(result: u32, entries: &[u32]) -> Option<(u32, u32)> {
    for (i, &x) in entries.iter().enumerate() {
        for &y in &entries[i + 1..] {
            if x + y == result {
                return Some((x, y))
            }
        }
    }
    None
}

/// Find the three numbers in the slice that sum to the `result`.
///
/// Returns an option containing these numbers, or None if none were found.
fn find_sum_3(result: u32, entries: &[u32]) -> Option<(u32, u32, u32)>{
    for (i, &x) in entries.iter().enumerate() {
        if let Some((y, z)) = find_sum_2(result - x, &entries[i + 1..]) {
            return Some((x, y, z));
        }
    }
    None
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_find_sum_2() {
        let found = find_sum_2(2020, &[1721, 979, 366, 299, 675, 1456]).unwrap();
        assert_eq!(found.0 * found.1, 514579);
    }

    #[test]
    fn test_find_sum_3() {
        let found = find_sum_3(2020, &[1721, 979, 366, 299, 675, 1456]).unwrap();
        assert_eq!(found.0 * found.1 * found.2, 241861950);
    }
}
