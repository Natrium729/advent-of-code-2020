//! Solves the day 8.
//!
//! https://adventofcode.com/2020/day/8

use std::collections::HashSet;
use std::fs;

pub fn solve() {
    let code = fs::read_to_string("resources/boot-code.txt")
        .expect("boot-code.txt does not exist");
    let code = process_code(&code);

    println!("Answer 8.1: {}", run_code(&code).unwrap_err());
    println!("Answer 8.2: {}", fix_code(&code));
}

/// The possible opcode of our virtual machine.
#[derive(Debug, Copy, Clone)]
enum Opcode {
    Nop(i32),
    Acc(i32),
    Jmp(i32),
}

/// Transform a string whose lines are instructions into a vec of Opcodes.
fn process_code(code: &str) -> Vec<Opcode> {
    code.lines()
        .map(|line| {
            let mut iter = line.split_whitespace();
            let op_str = iter.next().unwrap();
            let arg = iter.next().unwrap().parse::<i32>().unwrap();
            match op_str {
                "nop" => Opcode::Nop(arg),
                "acc" => Opcode::Acc(arg),
                "jmp" => Opcode::Jmp(arg),
                _ => panic!("invalid opcode encountered: {}", op_str)
            }
        })
        .collect::<Vec<_>>()
}

/// Run the given program.
///
/// If an out of bounds instruction is executed, it will panic.
///
/// If there is a infinite loop, an `Err` containing the value of the program before
/// executing the second loop is returned.
///
/// If the program reaches the end of its instructions, an `Ok` containing the returned
/// value of the program is returned.
fn run_code(code: &[Opcode]) -> Result<i32, i32> {
    let mut cursor: i32 = 0;
    let mut acc = 0;
    let mut visited = HashSet::new();
    loop {
        if visited.contains(&cursor) {
            return Err(acc);
        } else if cursor as usize == code.len() {
            return Ok(acc);
        } else if cursor as usize > code.len() {
            panic!("cursor gone too far")
        }
        visited.insert(cursor);
        match code[cursor as usize] {
            Opcode::Nop(_) => cursor += 1,
            Opcode::Acc(arg) => {
                acc += arg;
                cursor += 1
            },
            Opcode::Jmp(arg) => cursor += arg,
        }
    }
}

/// Transform a program with an infinite loop into a working program.
///
/// The function tries swapping a `Nop` for `Jmp` (or vice versa) and checks if the program
/// then works. If it doesn't, it tries with another line.
///
/// The return value of the program is returned.
///
/// If the program can't be fixed, the function panics.
fn fix_code(code: &[Opcode]) -> i32 {
    // This cloned program is the one that will be modified.
    let mut patched = code.to_vec();

    // We loop through each line, change them one by one, and run the code for each change.
    // I'm sure there are more efficient way to find the faulty line, but anyway.
    for (line, opcode) in code.iter().enumerate() {
        match opcode {
            Opcode::Nop(arg) => {
                patched[line] = Opcode::Jmp(*arg);
                if let Ok(val) = run_code(&patched) { return val; };
                patched[line] = Opcode::Nop(*arg); // Revert the change.
            },
            Opcode::Jmp(arg) => {
                patched[line] = Opcode::Nop(*arg);
                if let Ok(val) = run_code(&patched) { return val; };
                patched[line] = Opcode::Jmp(*arg); // Revert the change.
            },
            _ => ()
        }
    }
    panic!("the code can't be fixed");
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test() {
        let code = "nop +0\n\
            acc +1\n\
            jmp +4\n\
            acc +3\n\
            jmp -3\n\
            acc -99\n\
            acc +1\n\
            jmp -4\n\
            acc +6";
        let code = process_code(code);
        assert_eq!(run_code(&code).unwrap_err(), 5);
        assert_eq!(fix_code(&code), 8);
    }
}
