//! Solves the day 3.
//!
//! https://adventofcode.com/2020/day/3

use std::fs;

pub fn solve() {
    let map = fs::read_to_string("resources/map.txt")
        .expect("map.txt does not exist");
    println!("Answer 3.1: {}", count_trees(&map[..], 3, 1));

    let count = count_trees(&map[..], 1, 1)
        * count_trees(&map[..], 3, 1)
        * count_trees(&map[..], 5, 1)
        * count_trees(&map[..], 7, 1)
        * count_trees(&map[..], 1, 2);
        println!("Answer 3.2: {}", count);
}

/// Count the trees (marked as #) given a right and down speed.
fn count_trees(map: &str, right: u32, down: u32) -> usize {
    map
    .lines()
    .step_by(down as usize)
    .scan(0, |state, line| {
        let bytes = line.as_bytes(); // We assume the file in in ASCII.
        let ch = bytes[(*state as usize) % bytes.len()];
        *state += right;
        Some(ch as char)
    })
    .filter(|&ch| ch == '#')
    .count()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_count_trees() {
        let map = "..##.......\n\
            #...#...#..\n\
            .#....#..#.\n\
            ..#.#...#.#\n\
            .#...##..#.\n\
            ..#.##.....\n\
            .#.#.#....#\n\
            .#........#\n\
            #.##...#...\n\
            #...##....#\n\
            .#..#...#.#\n";
        assert_eq!(count_trees(map, 3, 1), 7);
    }

    #[test]
    fn test_slopes() {
        let map = "..##.......\n\
            #...#...#..\n\
            .#....#..#.\n\
            ..#.#...#.#\n\
            .#...##..#.\n\
            ..#.##.....\n\
            .#.#.#....#\n\
            .#........#\n\
            #.##...#...\n\
            #...##....#\n\
            .#..#...#.#\n";
        let slope1 = count_trees(map, 1, 1);
        assert_eq!(slope1, 2);
        let slope2 = count_trees(map, 3, 1);
        assert_eq!(slope2, 7);
        let slope3 = count_trees(map, 5, 1);
        assert_eq!(slope3, 3);
        let slope4 = count_trees(map, 7, 1);
        assert_eq!(slope4, 4);
        let slope5 = count_trees(map, 1, 2);
        assert_eq!(slope5, 2);
    }
}
