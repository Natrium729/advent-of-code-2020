//! Solves the day 4.
//!
//! https://adventofcode.com/2020/day/4

use std::fs;

pub fn solve() {
    // count.0 is the number of passports having all required fields.
    // count.1 is the number of passports having all required fields, all with valid values.
    let count = fs::read_to_string("resources/passports.txt")
        .expect("passports.txt does not exist")
        .split("\n\n") // We assume only LF are used (there are no CRLF).
        .fold((0u32, 0u32), |acc, x| {
            let is_valid = passport_is_valid(x);
            (
                acc.0 + is_valid.0 as u32,
                acc.1 + (is_valid.0 && is_valid.1) as u32
            )
        });
    println!("Answer 4.1: {}", count.0);
    println!("Answer 4.2: {}", count.1);
}

/// Check if `value` is valid for a field of type `key`.
fn check_field(key: &str, value: &str) -> bool {
    match key {
        "byr" => {
            let year = value.parse::<u32>().unwrap_or(0); // 0 will be invalid.
            year >= 1920 && year <= 2002
        },
        "iyr" => {
            let year = value.parse::<u32>().unwrap_or(0); // 0 will be invalid.
            year >= 2010 && year <= 2020
        },
        "eyr" => {
            let year = value.parse::<u32>().unwrap_or(0); // 0 will be invalid.
            year >= 2020 && year <= 2030
        },
        "hgt" => {
            if let Some(height) = value.strip_suffix("cm") {
                let height = height.parse::<u32>().unwrap_or(0); // 0 will be invalid.
                return height >= 150 && height <= 193;
            } else if let Some(height) = value.strip_suffix("in") {
                let height = height.parse::<u32>().unwrap_or(0); // 0 will be invalid.
                return height >= 59 && height <= 76;
            }
            false
        }
        "hcl" => {
            if let Some(colour) = value.strip_prefix('#') {
                let mut all_chars_valid = true;
                let count = colour.chars()
                    .fold(0u32, |acc, ch| {
                        if !ch.is_ascii_hexdigit() { all_chars_valid = false }
                        acc + 1
                    });
                return count == 6 && all_chars_valid;
            }
            false
        },
        "ecl" => value == "amb"
            || value == "blu"
            || value == "brn"
            || value == "gry"
            || value == "grn"
            || value == "hzl"
            || value == "oth",
        "pid" => {
            let mut all_chars_valid = true;
            let count = value.chars()
                .fold(0, |acc, ch| {
                    if !ch.is_ascii_digit() { all_chars_valid = false }
                    acc + 1
                });
            count == 9 && all_chars_valid
        }
        "cid" => true, // We ignore the "cid" field (i.e. we always consider it correct).
        _ => true // We return true to ignore any unknown field (we don't care about them).
    }
}

/// Determine if a passport is valid.
///
/// The first bool tells if the passport has all required fields.
/// The second tells if all the fields present have valid values.
fn passport_is_valid(passport: &str) -> (bool, bool) {
    let mut all_fields_valid = true;
    let keys = passport.split_whitespace()
        .map(|x| {
            let mut iter = x.split(':');
            let key = iter.next().unwrap();
            if !check_field(key, iter.next().unwrap()) { all_fields_valid = false };
            key
        })
        .collect::<Vec<_>>();
    let has_all_fields = keys.contains(&"byr")
        && keys.contains(&"iyr")
        && keys.contains(&"eyr")
        && keys.contains(&"hgt")
        && keys.contains(&"hcl")
        && keys.contains(&"ecl")
        && keys.contains(&"pid");
    (has_all_fields, all_fields_valid)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_passport_all_fields_present() {
        let passports = "ecl:gry pid:860033327 eyr:2020 hcl:#fffffd\n\
        byr:1937 iyr:2017 cid:147 hgt:183cm\n\
        \n\
        iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884\n\
        hcl:#cfa07d byr:1929\n\
        \n\
        hcl:#ae17e1 iyr:2013\n\
        eyr:2024\n\
        ecl:brn pid:760753108 byr:1931\n\
        hgt:179cm\n\
        \n\
        hcl:#cfa07d eyr:2025 pid:166559648\n\
        iyr:2011 ecl:brn hgt:59in".split("\n\n")
            .collect::<Vec<_>>();

        assert!(passport_is_valid(passports[0]).0);
        assert!(!passport_is_valid(passports[1]).0);
        assert!(passport_is_valid(passports[2]).0);
        assert!(!passport_is_valid(passports[3]).0);
    }

    #[test]
    fn test_check_field() {
        // The assertions marked with * were not given as examples but added by myself.
        // They failed when I solved the puzzle, because luckily such cases did not appear.
        // But for correctness I decided to handle them.
        assert!(check_field("byr", "2002"));
        assert!(!check_field("byr", "2003"));
        assert!(check_field("hgt", "60in"));
        assert!(check_field("hgt", "190cm"));
        assert!(!check_field("hgt", "190in"));
        assert!(!check_field("hgt", "190"));
        assert!(check_field("hcl", "#123abc"));
        assert!(!check_field("hcl", "#123abz"));
        assert!(!check_field("hcl", "#z123abc")); // *
        assert!(!check_field("hcl", "123abc"));
        assert!(check_field("ecl", "brn"));
        assert!(!check_field("ecl", "wat"));
        assert!(check_field("pid", "000000001"));
        assert!(!check_field("pid", "0000000019")); // *
        assert!(!check_field("pid", "0123456789"));
    }

    #[test]
    fn test_passport_all_fields_present_valid() {
        // All those are invalid.
        "eyr:1972 cid:100\n\
        hcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926\n\
        \n\
        iyr:2019\n\
        hcl:#602927 eyr:1967 hgt:170cm\n\
        ecl:grn pid:012533040 byr:1946\n\
        \n\
        hcl:dab227 iyr:2012\n\
        ecl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277\n\
        \n\
        hgt:59cm ecl:zzz\n\
        eyr:2038 hcl:74454a iyr:2023\n\
        pid:3556412378 byr:2007".split("\n\n")
            .for_each(|passport| {
                let validity = passport_is_valid(passport);
                assert!(!validity.0 || !validity.1)
            });

        // All those are invalid.
        "pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980\n\
        hcl:#623a2f\n\
        \n\
        eyr:2029 ecl:blu cid:129 byr:1989\n\
        iyr:2014 pid:896056539 hcl:#a97842 hgt:165cm\n\
        \n\
        hcl:#888785\n\
        hgt:164cm byr:2001 iyr:2015 cid:88\n\
        pid:545766238 ecl:hzl\n\
        eyr:2022\n\
        \n\
        iyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719".split("\n\n")
        .for_each(|passport| {
            let validity = passport_is_valid(passport);
            assert!(validity.0 && validity.1)
        });
    }
}
