//! Solves the day 7.
//!
//! https://adventofcode.com/2020/day/7

use std::collections::{HashMap, HashSet};
use std::fs;

pub fn solve() {
    let rules = fs::read_to_string("resources/luggage-rules.txt")
        .expect("luggage-rules.txt does not exist");

    // The bags and the bags they should contain.
    // The keys are Bags.
    // Each value is a Vec containing the bags the key contains, as well as the numbers it
    // should contain.
    let mut bags = HashMap::new();

    for rule in rules.lines() {
        let (container, contained) = parse_rule(rule);
        bags.insert(container, contained);
    }

    // The shiny gold bag.
    let shiny_gold_bag = Bag {
        modifier: String::from("shiny"),
        colour: String::from("gold"),
    };
    let shiny_gold_ancestors = find_ancestors(&shiny_gold_bag, &bags);
    println!("Answer 7.1: {}", shiny_gold_ancestors.len());
    println!("Answer 7.2: {}", number_of_bags_in(&shiny_gold_bag, &bags));
}

/// Represents a type of bag: a modifier and a colour.
#[derive(Debug, Eq, PartialEq, Hash)]
struct Bag {
    colour: String,
    modifier: String,
}

/// Parse a rule.
///
/// The first element of the tuple is the containing bag.
///
/// The second is the Vec of contained bags and their respective numbers.
fn parse_rule(rule: &str) -> (Bag, Vec<(Bag, u32)>) {
    let mut words = rule.split_whitespace();

    // Get the containing bag.
    let container_modifier = words.next().unwrap(); // e.g. "shiny".
    let container_colour = words.next().unwrap(); // e.g. "gold".
    if words.next().unwrap() != "bags" {
        panic!("the third word should be 'bags'")
    }
    if words.next().unwrap() != "contain" {
        panic!("the fourth word should be 'contain'");
    }
    let container = Bag {
        colour: String::from(container_colour),
        modifier: String::from(container_modifier),
    };

    // Get the contained bags.
    let mut contained = Vec::new();
    loop {
        let next = words.next();
        if next == None { // No more words: the rule is finished.
            break;
        }
        let number = next.unwrap();
        if number == "no" { // The rest is "no other bags" The bag contains no bags.
            break;
        }
        let number = number.parse::<u32>().unwrap();
        let modifier = words.next().unwrap();
        let colour = words.next().unwrap();
        // We remove the trailing comma or period from the next word, "bags"
        let bag_word = words.next().unwrap().trim_end_matches::<&[_]>(&[',', '.']);
        if bag_word != "bags" && bag_word != "bag" {
            panic!("the third word should be 'bags' or 'bag'")
        }
        contained.push((
            Bag {
                colour: String::from(colour),
                modifier: String::from(modifier),
            },
            number
        ))
    }

    (container, contained)
}

/// Find every bag that contain the target with the given bag rules.
fn find_parents<'a>(target: &Bag, bags: &'a HashMap<Bag, Vec<(Bag, u32)>>) -> HashSet<&'a Bag> {
    let mut parents = HashSet::new();

    for (container, contained) in bags.iter() {
        for (contained_bag, _quantity) in contained {
            if contained_bag == target {
                parents.insert(container);
                break
            }
        }
    }

    parents
}

/// Find all the bags that contain, directly or indirectly, the target with the given bag rules.
fn find_ancestors<'a>(target: &Bag, bags: &'a HashMap<Bag, Vec<(Bag, u32)>>) -> HashSet<&'a Bag> {
    let mut ancestors = HashSet::new();

    // Find the ancestors of the parents, recursively.
    // Not very efficient since we walk the whole set of bags each times, but it'll do.
    // We could remove the found bags to shorten the walk at each iteration, but that would
    // mutate the bag rules; in that case, we'd have to make a copy.
    // We'll let it be as it is.
    let parents = find_parents(target, bags);
    for parent in parents {
        ancestors.insert(parent);
        for parent_ancestor in find_ancestors(parent, bags) {
            ancestors.insert(parent_ancestor);
        }
    }
    ancestors
}

/// Find the number of bags contained in the the target according to the given bag rules.
fn number_of_bags_in(target: &Bag, bags: &HashMap<Bag, Vec<(Bag, u32)>>) -> u32 {
    bags.get(target).unwrap().iter()
        .map(|(bag, quantity)| quantity + quantity * number_of_bags_in(bag, bags))
        .sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test() {
        let rules = "light red bags contain 1 bright white bag, 2 muted yellow bags.\n\
        dark orange bags contain 3 bright white bags, 4 muted yellow bags.\n\
        bright white bags contain 1 shiny gold bag.\n\
        muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.\n\
        shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.\n\
        dark olive bags contain 3 faded blue bags, 4 dotted black bags.\n\
        vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.\n\
        faded blue bags contain no other bags.\n\
        dotted black bags contain no other bags."
            .lines();

        // Find every kind of bag that can be an ancestor of a shiny gold bag.

        let mut bags = HashMap::new();
        for rule in rules {
            let (container, contained) = parse_rule(rule);
            bags.insert(container, contained);
        }

        let shiny_gold_bag = Bag {
            modifier: String::from("shiny"),
            colour: String::from("gold"),
        };
        let shiny_gold_ancestors = find_ancestors(&shiny_gold_bag, &bags);

        assert_eq!(shiny_gold_ancestors.len(), 4);

        // Find the number of bags that has to be contained in a shiny gold bag.

        assert_eq!(number_of_bags_in(&shiny_gold_bag, &bags), 32);
    }
}
