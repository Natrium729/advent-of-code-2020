//! Solves the day 15.
//!
//! https://adventofcode.com/2020/day/15

use std::collections::HashMap;

pub fn solve() {
    let mut game = MemoryGame::new(vec![18, 8, 0, 5, 4, 1, 20]);
    println!("Answer 15.1: {}", game.nth(2019).unwrap());

    // It's a bit long to compute (~25-30 seconds on my computer), so I guess there's a
    // better way to find the answer than to just run the game up to the 30 000 000th
    // iteration. Still, the delay isn't unbearable, so we'll do with it.
    let mut game = MemoryGame::new(vec![18, 8, 0, 5, 4, 1, 20]);
    println!("Answer 15.2: {}", game.nth(29_999_999).unwrap());
}

/// An infinite iterator returning the result of each turn of the Elves' game.
struct MemoryGame {
    last_number: u32,
    numbers: HashMap<u32, u32>,
    start: Vec<u32>,
    turn: u32,
}

impl MemoryGame {
    /// Creates a new game starting with the given numbers.
    fn new(start: Vec<u32>) -> Self {
        MemoryGame {
            last_number: 0,
            numbers: HashMap::new(),
            start,
            turn: 0,
        }
    }
}

impl Iterator for MemoryGame {
    type Item = u32;

    fn next(&mut self) -> Option<Self::Item> {
        let previous_number = self.last_number;
        self.turn += 1;

        if let Some(&x) = self.start.first() {
            self.start.remove(0);
            self.last_number = x;
        } else if let Some(x) = self.numbers.get(&previous_number) {
            self.last_number = self.turn - 1 - x;
        } else {
            self.last_number = 0
        }

        self.numbers.insert(previous_number, self.turn - 1);
        Some(self.last_number)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_2020() {
        let mut game = MemoryGame::new(vec![0, 3, 6]);
        assert_eq!(game.next(), Some(0));
        assert_eq!(game.next(), Some(3));
        assert_eq!(game.next(), Some(6));
        assert_eq!(game.next(), Some(0));
        assert_eq!(game.next(), Some(3));
        assert_eq!(game.next(), Some(3));
        assert_eq!(game.next(), Some(1));
        assert_eq!(game.next(), Some(0));
        assert_eq!(game.next(), Some(4));
        assert_eq!(game.next(), Some(0));

        let mut game = MemoryGame::new(vec![0, 3, 6]);
        assert_eq!(game.nth(2019), Some(436));
    }

    #[test]
    fn test_30000000() {
        let mut game = MemoryGame::new(vec![0, 3, 6]);
        assert_eq!(game.nth(29_999_999), Some(175594));
    }
}
