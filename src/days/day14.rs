//! Solves the day 14.
//!
//! https://adventofcode.com/2020/day/14

use std::collections::HashMap;
use std::fs;

pub fn solve() {
    let program = fs::read_to_string("resources/init-program.txt")
        .expect("init-program.txt does not exist");

    let mut decoder = FirstDecoder::new();
    decoder.run(&program);
    println!("Answer 14.1: {}", decoder.memory.values().sum::<u64>());

    let mut decoder = SecondDecoder::new();
    decoder.run(&program);
    println!("Answer 14.2: {}", decoder.memory.values().sum::<u64>());
}

trait Decoder {
    /// Creates a Decoder with an empty memory and no mask.
    fn new() -> Self;

    /// Runs the program represented by the given string.
    fn run(&mut self, instructions: &str) {
        for line in instructions.lines() {
            let mut parts = line.split(" = ");
            let op = parts.next().unwrap();
            let value = parts.next().unwrap();
            if op == "mask" {
                self.set_mask(value)
            } else if let Some(x) = op.strip_prefix("mem[") {
                // Strip the closing bracket.
                let address = x[0..x.len() - 1].parse().unwrap();
                self.write(address, value.parse().unwrap());
            }
        }
    }

    /// Sets the mask of the Decoder to the given value.
    fn set_mask(&mut self, mask: &str);

    /// Write a value into the Decoder's memory, taking account of its mask.
    fn write(&mut self, address: u64, value: u64);
}

/// Represents a decoder that can read the program that will write to its memory.
///
/// The mask is applied to values before writing them to memory.
struct FirstDecoder {
    /// The memory of the decoder. Non-present values are considered to be 0.
    memory: HashMap<u64, u64>,
    /// The current mask applied to a value before it is written in memory. 
    mask: String,
}

impl Decoder for FirstDecoder {
    /// Creates a FirstDecoder with an empty memory and no mask.
    fn new() -> FirstDecoder {
        FirstDecoder {
            memory: HashMap::new(),
            mask: String::with_capacity(36),
        }
    }

    /// Sets the mask of the Decoder to the given value.
    fn set_mask(&mut self, mask: &str) {
        self.mask.clear();
        self.mask.push_str(mask);
    }

    /// Applies the Decoder's current mask to the value and writes it to the address in the Decoder's memory.
    ///
    /// Panics if the mask is empty.
    fn write(&mut self, address: u64, value: u64) {
        if self.mask.is_empty() {
            panic!("trying to write while the mask is not set");
        }

        // `zeroing` is used to put zeros in a number at the specified places of the mask.
        // Its bits are all set to 1 except for the ones that have a value in the mask.
        // `applying` is used to replace the specified places in a number with the content of the mask.
        // Its bits are all set to 0, except the ones that have a value in the mask, which are set to the same value.
        let (zeroing, applying) = self.mask.bytes()
            .rev()
            .enumerate()
            .fold((0u64, 0u64), |acc, (i, bit)| {
                let mut zeroing = acc.0;
                let mut applying = acc.1;
                if bit == b'X' {
                    zeroing += 1 << i;
                    applying += 0 << i;
                } else if bit == b'0' {
                    zeroing += 0 << i;
                    applying += 0 << i;
                } else if bit == b'1' {
                    zeroing += 0 << i;
                    applying += 1 << i;
                } else {
                    panic!("invalid mask byte: {}", bit);
                }
                (zeroing, applying)
            });

        self.memory.insert(address, (value & zeroing) + applying);
    }
}

/// Represents a decoder that can read the program that will write to its memory.
///
/// The mask is applied to addresses before writing a value to memory.
struct SecondDecoder {
    /// The memory of the decoder. Non-present values are considered to be 0.
    memory: HashMap<u64, u64>,
    /// The current mask applied to a value before it is written in memory.
    mask: String,
}

impl Decoder for SecondDecoder {
    /// Creates a SecondDecoder with an empty memory and no mask.
    fn new() -> SecondDecoder {
        SecondDecoder {
            memory: HashMap::new(),
            mask: String::with_capacity(36),
        }
    }

    /// Sets the mask of the Decoder to the given value.
    fn set_mask(&mut self, mask: &str) {
        self.mask.clear();
        self.mask.push_str(mask);
    }

    /// Writes the value to the address after applying the Decoder's mask to it.
    ///
    /// Panics if the mask is empty.
    fn write(&mut self, address: u64, value: u64) {
        if self.mask.is_empty() {
            panic!("trying to write while the mask is not set");
        }

        // Apply the mask to the address.
        // - 1 in the mask => 1 in the address
        // - 0 in the mask => digit in address unchanged.
        // - X in the mask => 0 in the address, but we store the position.
        let mut floating_bit_indices = Vec::new();
        let masked_address = self.mask.bytes()
            .rev() // To begin from the least significant bit.
            .enumerate()
            .fold(0u64, |acc, (i, b)| {
                let bit = match b {
                    b'X' => {
                        floating_bit_indices.push(i);
                        0
                    },
                    b'0' => (address & (1 << i)) >> i, // The digit i of the address unchanged.
                    b'1' => 1,
                    _ => panic!("invalid mask byte: {}", b)
                };
                acc + (bit << i)
            });

        // For each combination of digits to replace the Xs.
        for combination in 0..2u64.pow(floating_bit_indices.len() as u32) {
            let mut floating_address = masked_address;
            for (combination_position, &floating_position) in floating_bit_indices.iter().rev().enumerate() {
                // We take each digit of the combination at a time.
                let combination_mask = 1 << (combination_position);
                let combination_digit = (combination & combination_mask) >> combination_position;
                // We apply it at the correct position in the address.
                floating_address += combination_digit << floating_position;
            }
            self.memory.insert(floating_address, value);
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_first() {
        let program = "mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X\n\
            mem[8] = 11\n\
            mem[7] = 101\n\
            mem[8] = 0";
        let mut decoder = FirstDecoder::new();
        decoder.run(program);
        assert_eq!(decoder.memory.len(), 2);
        assert_eq!(decoder.memory[&7], 101);
        assert_eq!(decoder.memory[&8], 64);
    }

    #[test]
    fn test_second() {
        let program = "mask = 000000000000000000000000000000X1001X\n\
            mem[42] = 100\n\
            mask = 00000000000000000000000000000000X0XX\n\
            mem[26] = 1";
        let mut decoder = SecondDecoder::new();
        decoder.run(program);
        assert_eq!(decoder.memory.values().sum::<u64>(), 208);
    }
}
