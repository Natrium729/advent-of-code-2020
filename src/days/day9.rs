//! Solves the day 9.
//!
//! https://adventofcode.com/2020/day/9

use std::fs;
use std::cmp::Ordering;

pub fn solve() {
    let f = fs::read_to_string("resources/xmas-encoding.txt")
        .expect("xmas-encoding.txt does not exist");
    let entries = f.lines()
        .map(|x| x.parse::<u64>().unwrap())
        .collect::<Vec<_>>();

    let invalid_number = check_data(&entries, 25).unwrap();
    println!("Answer 9.1: {}", invalid_number);

    let terms = find_contiguous_sum(invalid_number, &entries);
    println!("Answer 9.2: {}", terms.iter().min().unwrap() + terms.iter().max().unwrap() );
}

/// Determine if the given number is the sum of two nuumber in the given list.
fn is_sum_of_two(result: u64, list: &[u64]) -> bool {
    for (i, &first) in list.iter().enumerate() {
        for &second in &list[i + 1..] {
            if first + second == result {
                return true;
            }
        }
    }
    false
}

/// Find the first number in `data` that is not the sum of two numbers in the previous
/// `preamble_size` entries.
fn check_data(data: &[u64], preamble_size: usize) -> Option<u64> {
    for (i, &x) in data.iter().enumerate().skip(preamble_size as usize) {
        if !is_sum_of_two(x, &data[i - preamble_size..i]) {
            return Some(x);
        }
    }
    None
}

/// Find the contiguous numbers in `entries` that sum to `result`.
fn find_contiguous_sum(result: u64, entries: &[u64]) -> Vec<u64> {
    let mut terms = Vec::new();
    for (i, _) in entries.iter().enumerate() {
        terms.clear();
        let mut so_far = 0;
        for &x in entries[i..].iter() {
            match so_far.cmp(&result) {
                Ordering::Greater => break,
                Ordering::Equal => return terms,
                Ordering::Less => {
                    so_far += x;
                    terms.push(x);
                }
            }
        }
    }
    panic!("couldn't find contiguous number that sum to {}", result);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test() {
        let numbers = vec![35, 20, 15, 25, 47, 40, 62, 55, 65, 95, 102, 117, 150, 182, 127, 219, 299, 277, 309, 576];
        let invalid_number = check_data(&numbers, 5).unwrap();
        assert_eq!(invalid_number, 127);
        let terms = find_contiguous_sum(invalid_number, &numbers);
        assert!(terms.contains(&15));
        assert!(terms.contains(&25));
        assert!(terms.contains(&47));
        assert!(terms.contains(&40));
    }
}
