//! Solves the day 5.
//!
//! https://adventofcode.com/2020/day/5

use std::fs;

pub fn solve() {
    let passes = fs::read_to_string("resources/boarding-passes.txt")
        .expect("boarding-passes.txt does not exist");
    let highest_id = passes.lines()
        .map(|line| {
            let (row, col) = find_seat(line);
            seat_id(row, col)
        })
        .max().unwrap();
    println!("Answer 5.1: {}", highest_id);

    let mut sorted_ids = passes.lines()
        .map(|line| {
            let (row, col) = find_seat(line);
            seat_id(row, col)
        })
        .collect::<Vec<_>>();
    sorted_ids.sort_unstable();
    let mut previous_id = sorted_ids[0] - 1;
    for id in sorted_ids {
        if id - previous_id == 2 { // We jumped an ID; it's must be ours, then.
            println!("Answer 5.2: {}", id - 1);
            break
        }
        previous_id = id
    }
}

/// Do a binary search for finding our seat.
///
/// The `instructions` are in the form `"(F|B){7}(L|R){3}"`
///
/// Return a tuple containing the row and the column.
fn find_seat(instructions: &str) -> (u32, u32) {
    let mut iter = instructions.chars();
    let row = iter.by_ref().take(7)
        .fold((0, 128), |acc, ch| { // 128 non-inclusive.
            let half = (acc.1 - acc.0) / 2;
            if ch == 'F' { // In the lower half.
                (acc.0, acc.1 - half)
            } else if ch == 'B' { // In the upper half.
                (acc.0 + half, acc.1)
            } else {
                panic!("expected F or B, got {}", ch)
            }
        })
        .0;
    let col = iter
        .fold((0, 8), |acc, ch| { // 8 non-inclusive.
            let half = (acc.1 - acc.0) / 2;
            if ch == 'L' { // In the lower half.
                (acc.0, acc.1 - half)
            } else if ch == 'R' { // In the upper half.
                (acc.0 + half, acc.1)
            } else {
                panic!("expected L or R, got {}", ch)
            }
        })
        .0;
    (row, col)
}

/// Calculate a seat's ID according to its row and column.
fn seat_id(row: u32, col: u32) -> u32 {
    row * 8 + col
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_find_seat() {
        assert_eq!(find_seat("FBFBBFFRLR"), (44, 5));
        assert_eq!(seat_id(44, 5), 357);
        assert_eq!(find_seat("BFFFBBFRRR"), (70, 7));
        assert_eq!(seat_id(70, 7), 567);
        assert_eq!(find_seat("FFFBBBFRRR"), (14, 7));
        assert_eq!(seat_id(14, 7), 119);
        assert_eq!(find_seat("BBFFBBFRLL"), (102, 4));
        assert_eq!(seat_id(102, 4), 820);
    }
}
