//! Solves the day 13.
//!
//! https://adventofcode.com/2020/day/13

use std::fs;

pub fn solve() {
    let notes = fs::read_to_string("resources/shuttle-bus.txt")
        .expect("shuttle-bus.txt does not exist");
    let (from, buses) = parse_notes(&notes);
    let earliest_bus = find_earliest_bus(from, &buses);

    println!("Answer 13.1: {}", earliest_bus.0 * (earliest_bus.1 - from));

    let time_with_consecutive_buses = find_consecutive_buses(
        &buses,
        // The start is hinted by Advent of Code, but in fact the algorithm is fast enough to
        // even start at 0.
        100_000_000_000_000
    );
    println!("Answer 13.2: {}", time_with_consecutive_buses);
}

/// Parses the puzzle input to get the earliest timestamp available and the buses IDs.
///
/// The buses marked "x" are given an ID of zero.
fn parse_notes(notes: &str) -> (u32, Vec<u32>) {
    let mut lines = notes.lines();
    let timestamp = lines.next().unwrap().parse().unwrap();
    let buses = lines.next().unwrap()
        .split(',')
        .map(|id| if id == "x" { 0 } else { id.parse().unwrap() })
        .collect();
    (timestamp, buses)
}

/// Find the earliest bus starting from the given time.
///
/// Returns the bus' ID and the time at which it comes.
fn find_earliest_bus(from: u32, buses: &[u32]) -> (u32, u32) {
    for time in from.. {
        for &bus in buses {
            if bus == 0 { // We skip out of service buses.
                continue;
            }
            if time % bus == 0 {
                return (bus, time);
            }
        }
    }
    // We normally cannot reach this point (because of the endless loop).
    panic!("no bus found");
}

/// Compute the greatest common divisor of the given numbers.
fn gcd(a: u64, b: u64) -> u64 {
    if b == 0 {
        return a;
    }
    gcd(b, a % b)
}

/// Compute the lowest common multiple of the given numbers.
fn lcm(a: u64, b: u64) -> u64 {
    if a == 0 && b == 0 {
        return 0
    }
    (a * b) / gcd(a, b)
}

/// Find the time at which each bus in the given vector arrives one after the other.
fn find_consecutive_buses(buses: &[u32], start: u64) -> u64 {
    // There's surely a more efficient algorithm but this one is fast enough!

    // Sort the buses by their ID (largest first), but also keep track of their original position.
    let mut buses = buses.iter()
        .map(|&x| x as u64)
        .enumerate()
        .filter(|&x| x.1 != 0) // Remove out of service buses.
        .collect::<Vec<_>>();
    buses.sort_unstable_by_key(|k| k.1);
    buses.reverse();

    // Get the bus with the largest ID.
    let (largest_rank, _largest_id) = buses.first().unwrap();

    let mut jump = 1; // We'll advance the time by this amount.
    let mut matched = 0; // The number of buses that departs around the time checked.

    // The time we'll check (it's the time the first bus of `buses` departs).
    // We subtract `jump` because it will be re-added at the start of the loop below.
    let mut time = start - jump;

    'outer: loop {
        if matched == buses.len() { // All the buses departed around the last time checked
            // We return the time of the first bus that departs, not of the first of `buses`.
            return time - *largest_rank as u64;
        }

        // We advance the time.
        time += jump;

        // We check each bus that have not matched. (We know the others are OK because they'll always be OK in an interval of `jump`.)
        for &(rank, id) in &buses[matched..] {
            let checked_time = time + rank as u64 - *largest_rank as u64;
            if (checked_time) % id == 0 { // It departs at this time!
                matched += 1;
                // All the buses will depart at an interval of the LCM of their ID, so we can
                // jump a bigger amount of time at each iteration.
                jump = lcm(jump, id);
            } else {
                continue 'outer;
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test() {
        let notes = "939\n\
            7,13,x,x,59,x,31,19";
        let (from, buses) = parse_notes(notes);
        let earliest_bus = find_earliest_bus(from, &buses);
        assert_eq!(earliest_bus.0, 59);
        assert_eq!(earliest_bus.1, 944);

        let cycle = buses.iter()
            .map(|&x| x as u64)
            .filter(|&x| x != 0)
            .fold(1, |acc, x| lcm(acc as u64, x as u64));
        println!("cycle: {}", cycle);
        assert_eq!(find_consecutive_buses(&buses, 1_000_000), 1068781);
        assert_eq!(find_consecutive_buses(&[17, 0, 13 ,19], 3000), 3417);
        assert_eq!(find_consecutive_buses(&[67, 7, 59, 61], 700_000), 754018);
        assert_eq!(find_consecutive_buses(&[67,0, 7, 59, 61], 750_000), 779210);
        assert_eq!(find_consecutive_buses(&[67, 7, 0, 59, 61], 1_000_000), 1261476);
        assert_eq!(find_consecutive_buses(&[1789, 37, 47, 1889], 1_200_000_000), 1202161486);
    }
}
