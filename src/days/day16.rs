//! Solves the day 16.
//!
//! https://adventofcode.com/2020/day/16

use std::collections::HashMap;
use std::fs;

pub fn solve() {
    let notes = fs::read_to_string("resources/tickets.txt")
        .expect("tickets.txt does not exist");

    let (rules, _your_ticket, other_tickets) = parse_notes(&notes);

    let scanning_error_rate = other_tickets.iter()
        .map(|values| invalid_values(values, &rules))
        .flatten()
        .sum::<u32>();
    println!("Answer 16.1: {}", scanning_error_rate);

    // // It's a bit long to compute (~25-30 seconds on my computer), so I guess there's a
    // // better way to find the answer than to just run the game up to the 30 000 000th
    // // iteration. Still, the delay isn't unbearable, so we'll do with it.
    // let mut game = MemoryGame::new(vec![18, 8, 0, 5, 4, 1, 20]);
    // println!("Answer 15.2: {}", game.nth(29_999_999).unwrap());
}

fn parse_notes(notes: &str) -> (HashMap<String, Vec<(u32, u32)>>, Vec<u32>, Vec<Vec<u32>>) {
    let mut split_notes = notes.split("\n\n");
    let rules = parse_rules(split_notes.next().unwrap());
    let your_ticket = split_notes.next().unwrap()
        .lines().nth(1).unwrap(); // Skip the title
    let your_ticket = parse_ticket(your_ticket);

    let other_tickets = split_notes.next().unwrap()
        .lines().skip(1) // Skip the title
        .map(|ticket| parse_ticket(ticket))
        .collect::<Vec<_>>();

    (rules, your_ticket, other_tickets)
}

fn parse_rules(rules: &str) -> HashMap<String, Vec<(u32, u32)>> {
    rules.lines()
        .map(|line| {
            let mut split_line = line.split(": ");
            let name = split_line.next().unwrap();
            let ranges = split_line.next().unwrap()
                .split(" or ")
                .map(|range| {
                    let mut numbers = range.split('-');
                    let min = numbers.next().unwrap().parse().unwrap();
                    let max = numbers.next().unwrap().parse().unwrap();
                    (min, max)
                }).collect::<Vec<(u32, u32)>>();
            (String::from(name), ranges)
        }).collect()
}

fn parse_ticket(ticket: &str) -> Vec<u32> {
    ticket.split(',')
        .map(|number| number.parse().unwrap())
        .collect()
}

fn invalid_values(values: &[u32], rules: &HashMap<String, Vec<(u32, u32)>>) -> Vec<u32> {
    let mut invalid_values = Vec::new();
    for &number in values {
        let is_value_valid = rules.values()
            .flatten()
            .any(|&range| (range.0..=range.1).contains(&number));
        if !is_value_valid {
            invalid_values.push(number);
        }
    }
    invalid_values
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test() {
        let notes = "class: 1-3 or 5-7\n\
            row: 6-11 or 33-44\n\
            seat: 13-40 or 45-50\n\
            \n\
            your ticket:\n\
            7,1,14\n\
            \n\
            nearby tickets:\n\
            7,3,47\n\
            40,4,50\n\
            55,2,20\n\
            38,6,12";

        let (rules, _your_ticket, other_tickets) = parse_notes(notes);

        let scanning_error_rate = other_tickets.iter()
            .map(|values| invalid_values(values, &rules))
            .flatten()
            .sum::<u32>();
        assert_eq!(scanning_error_rate, 71);
    }
}
