//! Solves the day 2.
//!
//! https://adventofcode.com/2020/day/2

use std::fs;

pub fn solve() {
    let answers = fs::read_to_string("resources/passwords.txt")
        .expect("passwords.txt does not exist")
        .lines()
        .map(|line| {
            let mut chars = line.chars();

            // Consume the first number.
            let min = chars.by_ref()
                .take_while(|ch| ch.is_digit(10))
                .collect::<String>()
                .parse::<usize>().unwrap();

            // Skip the dash and consume the second number.
            let max = chars.by_ref()
                .skip_while(|ch| !ch.is_digit(10))
                .take_while(|ch| ch.is_digit(10))
                .collect::<String>()
                .parse::<usize>().unwrap();

            // Skip the space and consume the letter.
            let letter = chars.by_ref()
                .find(|ch| ch.is_ascii_alphabetic())
                .unwrap().to_ascii_lowercase();

            // Skip the colon and consume the password.
            let password = chars.by_ref()
                .skip_while(|ch| !ch.is_ascii_alphabetic())
                .take_while(|ch| ch.is_ascii_alphabetic())
                .collect::<String>();

            // Return a tuple telling whether or not the password is valid according to each definition.
            (
                password_is_valid_count(&password[..], letter, min, max) as u32,
                password_is_valid_pos(&password[..], letter, min, max) as u32,
            )
        })
        .fold((0, 0), |(by_count, by_pos), x| (by_count + x.0, by_pos + x.1));

    println!("Answer 2.1: {}", answers.0);
    println!("Answer 2.2: {}", answers.1);
}

/// Determine if the given letter appears between `min` and `max` times in `password`.
fn password_is_valid_count(password: &str, letter: char, min: usize, max: usize) -> bool {
    let count = password.matches(letter).count();
    count >= min && count <= max
}

/// Determine if the given letter appears at either the index `pos1` or `pos2` in `password`.
fn password_is_valid_pos(password: &str, letter: char, pos1: usize, pos2: usize) -> bool {
    let chars: Vec<char> = password.chars().collect();
    (chars[pos1 -1] == letter) ^ (chars[pos2 -1] == letter)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_password_is_valid_count() {
        assert!(password_is_valid_count("abcde", 'a', 1, 3));
        assert!(!password_is_valid_count("cdefg", 'b', 1, 3));
        assert!(password_is_valid_count("ccccccccc", 'c', 2, 9));
    }

    #[test]
    fn test_password_is_valid_pos() {
        assert!(password_is_valid_pos("abcde", 'a', 1, 3));
        assert!(!password_is_valid_pos("cdefg", 'b', 1, 3));
        assert!(!password_is_valid_pos("ccccccccc", 'c', 2, 9));
    }
}
