mod days;

use crate::days::day1;
use crate::days::day2;
use crate::days::day3;
use crate::days::day4;
use crate::days::day5;
use crate::days::day6;
use crate::days::day7;
use crate::days::day8;
use crate::days::day9;
use crate::days::day10;
use crate::days::day11;
use crate::days::day12;
use crate::days::day13;
use crate::days::day14;
use crate::days::day15;
use crate::days::day16;

fn main() {
    let now = std::time::Instant::now();
    day1::solve();
    day2::solve();
    day3::solve();
    day4::solve();
    day5::solve();
    day6::solve();
    day7::solve();
    day8::solve();
    day9::solve();
    day10::solve();
    day11::solve();
    day12::solve();
    day13::solve();
    day14::solve();
    day15::solve();
    day16::solve();
    println!("Finished in {} ms.", now.elapsed().as_millis());
}
